# My project's README

Este repositorio es producto de la realización del trabajo final de carrera de Carlos Ariza para acceder al titulo de grado de Ingeniero electrónico con orientación en sistemas digitales.

El presente repositorio contiene:
-La librería OpenCV precompilada para el ARM del SoC ZC702.
-El núcleo de PetaLinux compilado y listo para el arranque desde memoria SD.
-El archivo TCL para el proyecto de VIVADO.
-Los archivos fuentes y el TCL correspondiente para el proyecto de Vivado HLS.
-Los archivos fuentes para realizar las pruebas desde Vivado SDK.
