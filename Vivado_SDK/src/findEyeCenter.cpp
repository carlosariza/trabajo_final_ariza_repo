/*
 * findEyeCenter.cpp
 *
 *  Created on: 23/11/2016
 *      Author: carlos Ariza
 * Este archivo define la función findEyeCenter(), que recibe la region de un ojo y realiza el calculo de los gradientes en la dimensión horizonal y vertical que son almacenados en sendas matrices. Luego con dichas matrices se envia al módulo IP generado con HLS mediante el mapeo de las direcciónes de memoria de los bloques RAM conectados al IP centro. Posteriormente el bloque IP retorna las coordenadas del centro del ojo. 
 */
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "include/xcentro.h"
//#include <time.h>

 using namespace cv;
 using namespace std;


// Pre-declarations
//direcciones fisicas

 #define XCENTRO_BASE_ADDRESS 0x43C00000//direccion fisica del IP centro

 #define AXI_BRAM_CONTROLER_0_BASE_ADRESS 0x40000000//gx direccion fisica del bram_ctroller asociado al bram
 #define AXI_BRAM_CONTROLER_1_BASE_ADRESS 0x42000000//gydireccion fisica del bram_ctroller asociado al bram

 //################################################################################
 #define MAP_SIZE 8192UL
 #define MAP_MASK (MAP_SIZE - 1)
 #define MAP_SIZE_CENTRO 4096UL
 #define MAP_MASK_CENTRO (MAP_SIZE_CENTRO - 1)
  //###############################################################################
 //Inicializacion del IP

 XCentro ip_centro;


cv::Point unscalePoint(cv::Point p, cv::Rect origSize) {
  float ratio = (((float)50)/origSize.width);
  int x = round(p.x / ratio);
  int y = round(p.y / ratio);
  return cv::Point(x,y);
}

void scaleToFastSize(const cv::Mat &src,cv::Mat &dst) {
  cv::resize(src, dst, cv::Size(50,30));//reescala el tamaño de la region del ojo 50x30
} //(((float)kFastEyeWidth)/src.cols) * src.rows)

cv::Mat computeMatXGradient(const cv::Mat &mat) {
	//printf("inicio de funcion calcula gradiente:%f \n",((clock()-init)/(double)CLOCKS_PER_SEC));
  cv::Mat out(mat.rows,mat.cols,CV_64F);

  for (int y = 0; y < mat.rows; ++y) {
    const uchar *Mr = mat.ptr<uchar>(y);
    double *Or = out.ptr<double>(y);

    Or[0] = Mr[1] - Mr[0];
    for (int x = 1; x < mat.cols - 1; ++x) {
      Or[x] = (Mr[x+1] - Mr[x-1])/2.0;
    }
    Or[mat.cols-1] = Mr[mat.cols-1] - Mr[mat.cols-2];
  }
  //printf("fin de funcion calcula gradiente :%f \n",((clock()-init)/(double)CLOCKS_PER_SEC));
  return out;
}

//##############################################################################

cv::Mat matrixMagnitude(const cv::Mat &matX, const cv::Mat &matY) {
  cv::Mat mags(matX.rows,matX.cols,CV_64F);
  for (int y = 0; y < matX.rows; ++y) {
    const double *Xr = matX.ptr<double>(y), *Yr = matY.ptr<double>(y);
    double *Mr = mags.ptr<double>(y);
    for (int x = 0; x < matX.cols; ++x) {
      double gX = Xr[x], gY = Yr[x];
      double magnitude = sqrt((gX * gX) + (gY * gY));
      Mr[x] = magnitude;
    }
  }
  return mags;
}

double computeDynamicThreshold(const cv::Mat &mat, double stdDevFactor) {
  cv::Scalar stdMagnGrad, meanMagnGrad;
  cv::meanStdDev(mat, meanMagnGrad, stdMagnGrad);
  double stdDev = stdMagnGrad[0] / sqrt(mat.rows*mat.cols);
  return stdDevFactor * stdDev + meanMagnGrad[0];
}

bool inMat(cv::Point p,int rows,int cols) {
  return p.x >= 0 && p.x < cols && p.y >= 0 && p.y < rows;
}

cv::Point findEyeCenter(cv::Mat face, cv::Rect eye, std::string debugWindow, float *prom_funcion_test, int *contador_test) {
	XCentro ip_centro;
	cv::Point maxP;
	int k=0;

	int memfd;//descriptor de fichero para BRAM_CTRL_0
	volatile void *mapped_base, *mapped_dev_base;//punteros para el mapeo de la pagina y del dispisitivo BRAM_CTRL_0
	off_t dev_base = AXI_BRAM_CONTROLER_0_BASE_ADRESS;//direccion base fisica

	int memfd1;//descriptor de fichero para BRAM_CTRL_1
	volatile void *mapped_base1, *mapped_dev_base1;//punteros para el mapeo de la pagina y del dispisitivo BRAM_CTRL_1
	off_t dev_base1 = AXI_BRAM_CONTROLER_1_BASE_ADRESS;//direccion base fisica

	int fd;//descriptor de fichero para IP_CENTRO
	volatile void *ptr_CENTRO_base,*ptr_CENTRO_dev_base ;//descriptor de fichero para IP_CENTRO
	off_t dev_base_centro = XCENTRO_BASE_ADDRESS;//direccion base fisica
  cv::Mat eyeROIUnscaled = face(eye);//genera una matriz con la region del ojo
  //cv::namedWindow("imroi2",CV_WINDOW_NORMAL);
  //imshow("imroi2",eyeROIUnscaled);
  cv::Mat eyeROI;
  scaleToFastSize(eyeROIUnscaled, eyeROI);//reescala la imagen del ojo

  // draw eye region
  ///rectangle(face,eye,1234);//dibuja el rectangulo de la zona del ojo en la cara en nivel de gris
  //-- Find the gradient
  cv::Mat gradientX = computeMatXGradient(eyeROI);//Calcula el gradiente en X
  cv::Mat gradientY = computeMatXGradient(eyeROI.t()).t();//calcula el gradiente en Y
  //-- Normalize and threshold the gradient
  // compute all the magnitudes
  //init=clock();
  cv::Mat mags = matrixMagnitude(gradientX, gradientY);//genera una matriz con la magnitud de los gradientes
  //printf("tiempo de funcion matrixMagnitude:%f \n",((clock()-init)/(double)CLOCKS_PER_SEC));
  //cv::namedWindow("imroi3",CV_WINDOW_NORMAL);
  // imshow("imroi3",mags);
  //compute the threshold
  double gradientThresh = computeDynamicThreshold(mags, 50.0);//calcula el umbral dinamico
  //double gradientThresh = kGradientThreshold;
  //double gradientThresh = 0;
  //normalize
  //init=clock();
  for (int y = 0; y < eyeROI.rows; ++y) {
    double *Xr = gradientX.ptr<double>(y), *Yr = gradientY.ptr<double>(y);
    const double *Mr = mags.ptr<double>(y);
    for (int x = 0; x < eyeROI.cols; ++x) {
      double gX = Xr[x], gY = Yr[x];
      double magnitude = Mr[x];
      if (magnitude > gradientThresh) {
        Xr[x] = gX/magnitude;//si la magnitud es mayor que el umbral
        Yr[x] = gY/magnitude;//sobre escribe los valores de gradiente por sus valores normalizados
      } else {
        Xr[x] = 0.0;//si no es mayor sobreescribe con 0.0
        Yr[x] = 0.0;
      }
    }
  }
  //printf("tiempo de for :%f \n",((clock()-init)/(double)CLOCKS_PER_SEC));
  //imshow(debugWindow,face);
  //-- Create a blurred and inverted image for weighting


  cv::Mat weight;
  //init=clock();
  GaussianBlur( eyeROI, weight, cv::Size( 5, 5 ), 0, 0 );
  for (int y = 0; y < weight.rows; ++y) {
    unsigned char *row = weight.ptr<unsigned char>(y);
    for (int x = 0; x < weight.cols; ++x) {
      row[x] = (255 - row[x]);
    }
  }
  //imshow(debugWindow,weight);
  //-- Run the algorithm!
  cv::Mat outSum = cv::Mat::zeros(eyeROI.rows,eyeROI.cols,CV_64F);
  // for each possible gradient location
  // Note: these loops are reversed from the way the paper does them
  // it evaluates every possible center for each gradient location instead of
  // every possible gradient location for every center.
  //printf("Eye Size: %ix%i\n",outSum.cols,outSum.rows);//imprime el tamaño del ojo

//buscar otra forma de hacer la asignaion
  //gradientX.assignTo(graX,double);
  memfd = open("/dev/mem", O_RDWR | O_SYNC);//apertura de /dev/mem
  	if (memfd == -1){
  		printf("Can't open /dev/mem.\n");
  	    exit(0);
  	}
  //	printf("/dev/mem opened.\n");
  	    //page_offset = (dev_base & (~MAP_MASK));
  	mapped_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memfd, dev_base & ~MAP_MASK);//mapeo
  	if (mapped_base == (void *) -1){
  		printf("Can't map the memory to user space.\n");
  	    exit(0);
  	 }
//  	 printf("memoria mapeada en la direccion %p.\n", mapped_base);
  	 mapped_dev_base = mapped_base + (dev_base & MAP_MASK);
  //	 printf("BRAM_CTRL_0 mapeada en la direccion %p.\n", mapped_dev_base);


  	memfd1 = open("/dev/mem", O_RDWR | O_SYNC);
  	if (memfd1 == -1){
  		printf("Can't open /dev/mem.\n");
  		exit(0);
  	}
  	//printf("/dev/mem opened.\n");
  		    //page_offset = (dev_base & (~MAP_MASK));
  	mapped_base1 = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memfd1, dev_base1 & ~MAP_MASK);
  	if (mapped_base1 == (void *) -1){
  		printf("Can't map the memory to user space.\n");
  		exit(0);
  	}
  //	printf("memoria mapeada en la direccion %p.\n", mapped_base1);
  	mapped_dev_base1 = mapped_base1 + (dev_base1 & MAP_MASK);
  	//printf("BRAM_CTRL_1 mapeada en la direccion %p.\n", mapped_dev_base1);
  for(int y=0;y<30;y++){
	  for(int x=0;x<50;x++){
		  //peso[y][x]=weight.at<unsigned char>(y,x);
		  *((float *)(mapped_dev_base + k)) = gradientX.at<double>(y,x);
		  *((float *)(mapped_dev_base1 + k))= gradientY.at<double>(y,x);
		  k=k+4;
		  //salida[y][x]=outSum.at<double>(y,x);
	  }
  }
  if (munmap((void *)mapped_base, MAP_SIZE) == -1) {
	  printf("Can't unmap memory from user space.\n");
	  exit(0);
  }
  if (munmap((void *)mapped_base1, MAP_SIZE) == -1) {
	  printf("Can't unmap memory from user space.\n");
  	  exit(0);
  }

  //printf("Termino de cargar las matrices de los gradientes \n");
 //##################################################################################################

  //float init=clock();
 // printf ( " Opening the dev memory %d \n " , sysconf( _SC_PAGESIZE ));
  	fd = open ( "/dev/mem" , O_RDWR | O_SYNC ) ;
  	if ( fd < 1) {
  		perror ( "Opening /dev/mem \n " );
  		exit ( -1) ;
  	} else
  	//printf ( " File open with %d index \n " , fd ) ;
  	ptr_CENTRO_base = mmap(0,MAP_SIZE_CENTRO, PROT_READ | PROT_WRITE, MAP_SHARED,fd , dev_base_centro & ~MAP_MASK_CENTRO);
  	if ( ptr_CENTRO_base == MAP_FAILED ) {
  		perror ( " Error with mmap \n");
  		exit (-1);
  	}
  //	printf("Memoria mapeada en la direccion %p.\n", ptr_CENTRO_base);

  	fflush(stdout);

  	//instanciación del bloque mediante la dirección obtedida del mapeo
  	ip_centro.IsReady=1;
  	ip_centro.Axilites_BaseAddress = (u32)ptr_CENTRO_base;
      XCentro_Start(&ip_centro);

      while(!XCentro_IsDone(&ip_centro)){
          	//printf("WHILE LOOP\n");
      }
      //maxP.y = XCentro_ReadReg(ip_centro.Axilites_BaseAddress, 0x10+5);
      //maxP.x = XCentro_ReadReg(ip_centro.Axilites_BaseAddress, 0x10+4);
      //printf("Coordenada x=%i  Coordenada y =%i  \n", maxP.x , maxP.y);

      maxP.y = XCentro_ReadReg(ip_centro.Axilites_BaseAddress, 0x10);
      maxP.x = XCentro_ReadReg(ip_centro.Axilites_BaseAddress, 0x10+4);

      //printf("Coordenada x=%i  Coordenada y =%i  \n", maxP.x , maxP.y);


      if (munmap((void *)ptr_CENTRO_base, MAP_SIZE_CENTRO) == -1) {
    	  printf("Can't unmap memory from user space.\n");
    	  exit(0);
      }
  //*prom_funcion_test = *prom_funcion_test + ((clock()-init)/(double)CLOCKS_PER_SEC);
  //(*contador_test)++;

//######################################################

  close(fd);
  close(memfd);
  close(memfd1);
  mags.release();
  outSum.release();
  weight.release();
  eyeROIUnscaled.release();
  eyeROI.release();
  face.release();
  return unscalePoint(maxP,eye);

}





