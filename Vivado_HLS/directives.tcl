############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
set_directive_pipeline "Centro/columna_centro"
set_directive_interface -mode bram "Centro" g_y
set_directive_interface -mode bram "Centro" g_x
set_directive_interface -mode s_axilite "Centro"
set_directive_latency "Centro/fila_grad"
set_directive_pipeline "Centro/Centro_label1"
set_directive_interface -mode s_axilite "Centro" s
