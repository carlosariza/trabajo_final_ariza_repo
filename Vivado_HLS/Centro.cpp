//#include <iostream>
#include "CentroH.h"
/*Este es el código fuente de la funcion testPossibleCentersFormula2 a ser sintetizado con Vivado HLS para obtener la implementación RTL.
  La funcion tiene como entrada 2 matrices con los gradientes en la dimensión horizontal y vertical, y como salida las coordenadas del centro del ojo*/
void Centro(
		grad_X g_x[ROWS][COLS],
		grad_Y g_y[ROWS][COLS],
		f s[2]
					   ) {
	int aux[2]={0,0};
	s[0]=0;
	s[1]=0;
	double gX, gY , dx, dy, magnitude, dotProduct, M =0.0;
	double sa[ROWS][COLS]={0};
	int f,c;
	fila_grad:for (int y = 0; y < ROWS; ++y) {
	    columnna_grad:for (int x = 0; x < COLS; ++x) {
	       gX= g_x[y][x];
	       gY = g_y[y][x];
	      if (gX == 0.0 && gY == 0.0) {
	        continue;
	      }
	      fila_centro:for (int cy = 0; cy < ROWS; ++cy) {
	        columna_centro:for (int cx = 0; cx < COLS; ++cx) {
	            if (x == cx && y == cy) {
	              continue;//continua por que es el mismo punto donde esta el gradiente
	            }
	            // crea un vector del posible centro al origen del gradiente
	            dx = x - cx;
	            dy = y - cy;
	            // normalize d
	            magnitude = sqrt((dx * dx) + (dy * dy));//calcula la magnitud
	            dx = dx / magnitude;//normaliza el vector distancia
	            dy = dy / magnitude;//
	            dotProduct = dx*gX + dy*gY;//calcula el producto punto
	            dotProduct = std::max(0.0,dotProduct);//los producto puntos negativos los hace 0 para asegurar que el vector sea radial hacia afuera
	            sa[cy][cx] += dotProduct * dotProduct;// * ((p[cy][cx])/1.0);
	            // square and multiply by the weight
	            /*if (False) {
	              s[cy][cx] += dotProduct * dotProduct * ((p[cy][cx])/1.0);
	            } else {
	              s[cy][cx] += dotProduct * dotProduct;
	            }*/
	          }
	        }
	    }
	  }// for all possible centers
	Centro_label0:for (int i=0;i<30;i++){
					Centro_label1:for(int j=0;j<50;j++){
					if(sa[i][j]>M){
						M=sa[i][j];
						aux[0]=i;
						aux[1]=j;
					}
				}
			}
	for(int i=0;i<2;i++){
		s[i]=aux[i];
	}


	}
