//Este archivo contiene la función principal del rastreador ocular, la cual define los objetos con los que se va a trabajar, como el archivo fuente de video, el archivo html del rastreador ocular etc.
//También en este archivo se define la función findEyes() que calcula la región de los ojos, con la cual se invoca 2 veces a la función findEyeCenter() que retorna las coordenadas del centro del ojo.

//prueba opencv2.4.9
#include <stdio.h>
#include <opencv2/opencv.hpp>
//#include <iostream>
//#include <math.h>
//#include <string.h>
#include "findEyeCenter.h"
//#include <time.h>//biblioteca de funciones para medir tiempos

using namespace std;
using namespace cv;

//VideoWriter wtr("video_de_salida.avi", CV_FOURCC('M', 'P', '4', '2'), 15, Size(1280,720));
//VideoWriter wtr("video_de_salida.avi", CV_FOURCC('M', 'J', 'P', 'G'), 25, Size(640,480));
//###########################################################################################


//###########################################################################################
void findEyes(cv::Mat frame ,cv::Mat frame_gray, cv::Rect face, float *sum, int *cont, float *p, int *q) {
  Mat faceROI = frame_gray(face);
  Mat debugFace = faceROI;
  //-- Find eye regions and draw them
  //de la region de la cara determina que fraccion es la region de los ojos
  int eye_region_width = face.width * (25/100.0);//ancho de la region del ojo = ancho de la cara *35/100##kEyePercentWidth=25
  int eye_region_height = face.width * (15/100.0);
  int eye_region_top = face.height * (30/100.0);
  cv::Rect leftEyeRegion(face.width*(20/100.0), eye_region_top,eye_region_width,eye_region_height);
  cv::Rect rightEyeRegion(face.width - eye_region_width - face.width*(20/100.0), eye_region_top,eye_region_width,eye_region_height);
    //printf("Calcula region de los ojos \n");
   // printf("Invoca a la funcion findEyeCenter \n");
    //-- Find Eye Centers
    //float ini=clock();
    cv::Point leftPupil = findEyeCenter(faceROI,leftEyeRegion,"Left Eye", p, q);//findEyeCenter hace toda la magia
    //*sum = *sum +((clock()-ini)/(double)CLOCKS_PER_SEC);
    //(*cont)++;
    //ini=clock();
    cv::Point rightPupil = findEyeCenter(faceROI,rightEyeRegion,"Right Eye",p ,q);
    //*sum = *sum +((clock()-ini)/(double)CLOCKS_PER_SEC);
    //(*cont)++;
    //printf("vuelve findEyeCenter \n");

  rightPupil.x = rightPupil.x + rightEyeRegion.x + face.x;
  //rightPupil.y += rightEyeRegion.y;
  rightPupil.y = rightPupil.y + rightEyeRegion.y + face.y;
  //leftPupil.x += leftEyeRegion.x;
  leftPupil.x = leftPupil.x + leftEyeRegion.x + face.x;
  //leftPupil.y += leftEyeRegion.y;
  leftPupil.y = leftPupil.y + leftEyeRegion.y + face.y;
  //printf("coordenadas del ojo derecho, x=%d y=%d \n",rightPupil.x, rightPupil.y);
  //printf("coordenadas del ojo izquierdo, x=%d y=%d \n",leftPupil.x, leftPupil.y);
  // draw eye centers
  circle(frame, rightPupil, 1.5, Scalar(0,255,0));
  circle(frame, leftPupil, 1.5,Scalar(0,255,0));
  imwrite("ojos.bmp", frame_gray);
 // wtr << frame;
  //printf("Graba un frame en gris con los ojos detectados \n");
  frame_gray.release();
  faceROI.release();
  debugFace.release();
  return;
}

int main(int argc, char** argv )
{

	VideoCapture cap;
	//VideoWriter wtr("video_de_salida.avi", CV_FOURCC('P','I','M','1'), 25, Size(640,480));
	Mat frame, gris;
	CascadeClassifier detector;
	float prom_carga_imagen = 0.0;
	int contador_cargas_imagen = 0;
	float prom_detectar_caras = 0.0;
	int contador_caras = 0;
	float prom_detectar_ojos = 0.0;
	int contador_ojos = 0;
	float prom_detectar_centro = 0.0;
	int contador_centro = 0;
	float prom_funcion_test = 0.0;
	int contador_test = 0;
	if (argc > 1){
		cap.open(string(argv[1]));//si se pasa algun archivo de video lo abre
		printf("Carga video pasado como parametro \n");
	}
	else {
		cap.open("video.avi");//si no hay parametro abre la webcam
		printf("Carga video por defecto \n");
	}
	if(!cap.isOpened()){// si no pudo abrir retorna -1 (error)
		printf("No se pudo abrir archivo de video \n");
		return -1;
	}
	//namedWindow("video",CV_WINDOW_NORMAL);//crea una ventana llamada "video"
	double fps = cap.get(CV_CAP_PROP_FPS);//obtener los cuadros por segundo
	cout<<"fps:"<<fps<<endl;
	// calcular el tiempo de espera entre cada imagen a mostrar
	//float delay = 100/ fps;// (142 ms /frame)calcular el tiempo de espera entre cada imagen a mostrar (es algo asi como la velocidad del video)
	//float ini=clock();
	if(!detector.load("haarcascade_frontalface_alt.xml"))//carga el clasificador desde el archivo, retorna un booleano
			cout << "No se puede abrir clasificador para las caras." << endl;
	//printf("tiempo en cargar el clasificador:%f \n",((clock()-ini)/(double)CLOCKS_PER_SEC));
	cap >> frame;
	//imwrite("primer_frame.bmp", frame);
	//printf("Graba el primer frame \n");
	std::vector<cv::Mat> rgbChannels(3);
	Rect rc;
	vector<Rect> rect;
	for(;;){
			//ini=clock();
			if(!cap.read(frame)){ //carga cada cuadro del video dentro de frame es equivalente a  cap.read(frame);
				/*printf("Termino de leer el video \n");
				printf("tiempo en cargar la imagen en promedio es : %f \n",prom_carga_imagen/contador_cargas_imagen);
				printf("tiempo en detectar caras en promedio es : %f \n",prom_detectar_caras/contador_caras);
				printf("tiempo en detectar ojos en promedio es : %f \n",prom_detectar_ojos/contador_ojos);
				printf("tiempo en detectar centro de los ojos en promedio es : %f \n",prom_detectar_centro/contador_centro);
				printf("tiempo de la funcion  a sintetizar en promedio es : %f \n",prom_funcion_test/contador_test);
				*/
				exit(0);
			}
			else{

				//prom_carga_imagen = prom_carga_imagen + ((clock()-ini)/(double)CLOCKS_PER_SEC);
				//contador_cargas_imagen++;

				cv::split(frame, rgbChannels);
				gris = rgbChannels[2];
				//ini=clock();
				//genera un vector para las caras detectadas
				detector.detectMultiScale(gris, rect, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE|CV_HAAR_FIND_BIGGEST_OBJECT, Size(150,150));//detecta objetos de diferentes tamaños en la imagen de entrada
				//printf("tiempo en detectar caras:%f \n",((clock()-ini)/(double)CLOCKS_PER_SEC));
				//prom_detectar_caras = prom_detectar_caras + ((clock()-ini)/(double)CLOCKS_PER_SEC);
				//contador_caras++;
				if(rect.size() > 0){
					rc = rect[0];
					rectangle(frame,rc,CV_RGB(255,255,255), 1);//dibuja el rectangulo que contiene la cara en frame
					//imwrite("cara.bmp", frame);
					//printf("Graba un frame con la cara detectada \n");
					//procesamientoojo(frame, dest ,rect);
					//ini=clock();
					findEyes(frame,gris, rect[0], &prom_detectar_centro, &contador_centro, &prom_funcion_test, &contador_test);//pasa la matriz de la imagen en escala de gris y la primer cara
					//prom_detectar_ojos = prom_detectar_ojos + ((clock()-ini)/(double)CLOCKS_PER_SEC);
					//contador_ojos++;


		}

		}
	}//end for
	frame.release();
	//wtr.release();
	gris.release();
	cap.release();
	return 0;
}//end main

