// este archivo cabecera define los tipos de los objetos y el prototipo de la función a implementar con Vivado HLS
#ifndef __TESTPOSSIBLECENTERFORMULA2_H__
#define __TESTPOSSIBLECENTERFORMULA2_H__

#include <cmath>
#include <stdio.h>
#include <iostream>
using namespace std;


// Uncomment this line to compare TB vs HW C-model and/or RTL
//#define HW_COSIM

#define ROWS 30
#define COLS 50


typedef unsigned char peso;
typedef float grad_X;
typedef float grad_Y;
typedef float salida;
typedef int f;
;
// Prototype of top level function for C-synthesis
void Centro(
		grad_X gx[ROWS][COLS],
		grad_Y gy[ROWS][COLS],
		f s[2]);


#endif // __ESTPOSSIBLECENTERFORMULA2_H__ not defined

