############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project centro_hls
set_top Centro
add_files src/CentroH.h
add_files src/Centro.cpp
add_files -tb src/test.cpp -cflags "-DHW_COSIM"
add_files -tb src/entradas.dat
open_solution "solution12"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
set_clock_uncertainty 1.25
source "./centro_hls/solution12/directives.tcl"
csim_design
csynth_design
cosim_design -trace_level all -rtl vhdl -tool xsim
export_design -evaluate vhdl -format ip_catalog -vendor "carlos" -display_name "centro"
